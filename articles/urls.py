from django.urls import path

from .views import (
    ArticleListView,
    ArticleUpdateView,
    ArticleDetailView,
    ArticleDeleteView,
    ArticleCreateView,
    ListArticle,
    DetailArticle,
)

urlpatterns = [
    path("<int:pk>/", DetailArticle.as_view()),
    path("", ListArticle.as_view()),
    path("<int:pk>/edit/", ArticleUpdateView.as_view(), name="article_edit"),
    path("<int:pk>/",ArticleDetailView.as_view(),name="article_detail"),
    path("<int:pk>/delete/", ArticleDeleteView.as_view(),name="article_delete"),
    path("new/", ArticleCreateView.as_view(),name="article_new"),
    path("",ArticleListView.as_view(), name="article_list"),

]
